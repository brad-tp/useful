<?php

namespace Owens\Useful;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    	require_once ( __DIR__ . '/Helpers/helpers.php');
    	View::addLocation(__DIR__ ."/../resources/views");
    }
}
