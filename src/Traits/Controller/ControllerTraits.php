<?php
namespace Owens\Useful\Traits\Controller;

use \Request;


trait ControllerTraits {
	function flashBackError($message) {
		if (Request::expectsJson()) {
			return response([
					'error'=> $message,
			]);
		}
		session()->flash('flash_error',$message);
		return redirect()->back();
	}
	
	function flashBackInfo($message) {
		if (Request::expectsJson()) {
			return response([
					'success'=> $message,
			]);
		}
		
		session()->flash('flash_info',$message);
		return redirect()->back();
	}
	
	function flashRouteInfo($message,$routeName,array $routeParams=[]) {
		if (Request::expectsJson()) {
			return response([
					'success'=> $message,
					'message' => $message,
					'link'=>route($routeName,$routeParams),
			]);
		}
		session()->flash('flash_info',$message);
		return redirect()->route($routeName,$routeParams);
	}
	
	function flashRouteError($message,$routeName,array $routeParams=[]) {
		if (Request::expectsJson()) {
			return response([
					'error'=> $message,
					'message' => $message,
					'link'=>route($routeName,$routeParams),
			]);
		}
		session()->flash('flash_error',$message);
		return redirect()->route($routeName,$routeParams);
	}
	
	function handleFail($failType,$message,...$args) {
		$method = 'handle' .studly_case(str_replace('.','_',$failType));
		if (method_exists($this,$method))
			return $this->{$method}($message,...$args);
			return $this->handleNoFailMethod($message,$args);
	}
	
	function handleNoFailMethod($message,$args) {
		return view('owens.stripe.fail',['message'=>$message]);
	}
	
	function callUserMethodWithDefault($method,$default,...$args) {
		if (method_exists($this,$method))
			return $this->{$method}(...$args);
		return $default;
	}
	
}