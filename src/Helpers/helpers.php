<?php 

function dynamic_layout($layout=false) {
	if (!isset($layout))
		$layout = 'layouts.login-noLeft';
	return $layout;
}

function array_key_extract($keys, $arr) {
	$result = [];
	foreach ($keys as $key) {
		if (isset($arr[$key]))
			$result[$key] = $arr[$key];
	}
	return $result;
}

function array_key_prefix($options, $prefix) {
	$new_array = [];
	foreach ($options as $key => $value)
		$new_array[$prefix.$key]= $value;
		return $new_array;
}

function array_key_extract_prefix($a, $prefix) {
	$len = strlen($prefix);
	$newArray = [];
	foreach ($a as $key=>$value) {
		if (strpos($key,$prefix) === 0) {
			$newArray[substr($key,$len)] = $value;
		}
	}
	return $newArray;
}

function category_path($category) {
	$categoryPath[] = $category->name;
	$cat = $category;
	while ($cat->parent) {
		$cat = $cat->parent;
		$categoryPath[] = $cat->name;
	}
	$categoryPath = array_reverse($categoryPath);
	return $categoryPath;
}

