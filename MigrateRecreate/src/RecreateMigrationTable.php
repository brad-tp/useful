<?php
namespace Thrasher\MigrateRecreate ;

use Owens\Traits\Migratable;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;


class RecreateMigrationTable {
	
	use Migratable;
	
	protected $filename;
	protected $table;
	protected $table2 = false;
	protected $id1 = false;
	protected $id2 = false;
	protected $name;
	protected $type = "table";
	protected $override = false;
	protected $views = [
			'table' => 'recreate_table',
			'many2many' => 'many2many_table',
			'nocreate' => 'nocreate_table',
	];
	protected $conn;
	
	function __construct($name,$table, $options = false, $filename = false) {
		$this->name=studly_case($name);
		$this->table = $table;
		if ($options)
			$this->setOptions($options);
		if (!$filename) {
			$datestr = Carbon::now()->format('Y_m_d_His');
			$this->filename = database_path("migrations/" .$datestr . "_" . $name. ".php");
		} else
			$this->filename = $filename;
		View::addLocation(__DIR__ ."/../stubs");
		$this->conn = Schema::getConnection()->getDoctrineSchemaManager();
		
	}
	
	function getView() {
		if (!isset($this->views[$this->type]))
			throw new \Exception("Invalid view type");
		return $this->views[$this->type];
	}
	
	function setOptions($options) {
		if (isset($options['type']))
			$this->type = $options['type'];
		if (isset($options['table2']))
			$this->table2 = $options['table2'];
		if (isset($options['overrride'])) 
			$this->override = $options['override'];
	}
	
	function getColumns($table) {
		$columns = DB::select('show columns from ' . $table);
		return $columns;
	}
	
	function getColumnDeclarations($table) {
		$columns = $this->getColumns($table);
		$declares = [];
		foreach ($columns as $column) {
			$declare = $this->getDeclaration($column);
			$declares[$declare['name']] = $declare;
		}
		return $declares;
	}
	
	function recreateMany2Many() {
		if (!$this->table2)
			throw new \Exception('Must provide --table2 when doing many 2 many relationship');
		// Order tables by Laravel standard.
		if ($this->table2 < $this->table) {
			$tmp = $this->table2;
			$this->table2 = $this->table;
			$this->table = $tmp;
			
			// If tables are swapped then ids must be swapped
			$tmp = $this->id1;
			$this->id1 = $this->id2;
			$this->id2 = $tmp;
		}
		
		$table_name = str_singular($this->table) . "_" . str_singular($this->table2);
		if ($this->override)
			$table_name = $this->override;
		$declares = $this->getColumnDeclarations($table_name);
		if ($this->id1)
			$params['id1'] = $this->id1;
		if ($this->id2)
			$params['id2'] = $this->id2;
		$params = [
				'table' => $table_name,
				'table1' => $this->table,
				'table2' => $this->table2,
				'columns' => $declares,
				'name' => $this->name,
		];
		$view = View::make($this->getView(),$params);
		return $view;
	}
	
	function getForeignKeys($table) {
		return $this->conn->listTableForeignKeys($table);
	}
	
	function getIndexes($table) {
		return $this->conn->listTableIndexes($table);
	}
	
	
	function recreateTable() {
		$declares = $this->getColumnDeclarations($this->table);
		$view =  View::make('recreate_table',[
				'table' => $this->table,
				'columns' => $declares,
				'name' => $this->name,
		]);
		return $view;
	}
	
	function arrayToString($a) {
		return "['" . implode('\',',$a) . "']";
	}
	
	function createForeignStr($foreign) {
		$str = "foreign(" . $this->arrayToString($foreign->getColumns()) . ")";
		$str .= "->references(" . $this->arrayToString($foreign->getForeignColumns()) .")";
		$str .= "->on('" . $foreign->getForeignTableName() . "')";
		if ($foreign->onUpdate())
			$str .= "->onUpdate('" .$foreign->onUpdate(). "')";
		if ($foreign->onDelete())
			$str .= "->onDelete('" .$foreign->onDelete(). "')";
						
		return $str;
	}
	
	function createIndexString($index) {
		if ($index->getName() == "PRIMARY")	// Should be created from creating Increment.
			return false;
		if ($this->isForeignKey($index->getName()))
			return false;
		if ($index->isUnique())
			$str = "index(" . $this->arrayToString($index->getColumns()) . ",'" .  $index->getName() ."')";
		else if ($index->isPrimary())
			echo "Is primary";
		else
			$str = "index(" . $this->arrayToString($index->getColumns()) . ",'" .  $index->getName() ."')";
		return $str;
	}
	
	function makeForeigns() {
		$f=[];
		$foreigns = $this->getForeignKeys($this->table);
		foreach($foreigns as $foreign) {
			$str = $this->createForeignStr($foreign);
			$f[] = $str;
		}
		return $f;
	}
	
	function isForeignKey($name) {
		$keys = $this->getForeignKeys($this->table);
		foreach ($keys as $key)
			if ($name == $key->getName())
				return true;
		return false;
	}
	
	function makeIndexes() {
		$idx = [];
		$indexes = $this->getIndexes($this->table);
		foreach ($indexes as $index) {
			$i  = $this->createIndexString($index);
			if ($i)
				$idx[] = $i;
		}
		return $idx;
	}
	
	function recreateNocreate() {
		$declares = $this->getColumnDeclarations($this->table);
		$f = $this->makeForeigns();
		$idx = $this->makeIndexes();
		$view =  View::make($this->getView(),[
				'table' => $this->table,
				'columns' => $declares,
				'name' => $this->name,
				'foreigns' => $f,
				'idxs' => $idx,
		]);
		return $view;
	}
	
	function recreate() {
		$fs = new Filesystem();
		$method = camel_case('recreate_' . $this->type);
		if (method_exists($this,$method))
			$view = $this->{$method}();	
		else 
			throw new \Exception("Bad recreate type: " . $this->type . " no method " . $method);
		$fs->put($this->filename,$view);
		
	}
	
	public static function createColumnMethodString($declare) {
		$column = "";
		switch ($declare['type']) {
			case 'float':
				$column = "float('" . $declare['name'] . "')";
				break;
			case 'double';
			$column = "double('" . $declare['name'] . "')";
			break;
			case 'bigint';
			if ($declare['extra'] == 'auto_increment')
				$column = "bigIncrements('" . $declare['name'] . "')";
				else
					$column = "bigInteger('" . $declare['name'] . "')";
					break;
			case 'integer';
			if ($declare['extra'] == 'auto_increment')
				$column = "Increments('" . $declare['name'] . "')";
				else
					$column = "integer('" . $declare['name'] . "')";
					break;
			case 'int';
			if ($declare['extra'] == 'auto_increment')
				$column = "Increments('" . $declare['name'] . "')";
				else
					$column = "integer('" . $declare['name'] . "')";
					break;
			case 'smallint';
			$column = "smallInteger('" . $declare['name'] . "')";
			break;
			
			case 'varchar':
				$column = "string('" . $declare['name'] . "'," . $declare['size'] . ")";
				break;
			case 'char':
				$column = "char('" . $declare['name'] . "'," . $declare['size'] . ")";
				break;
			case 'timestamp':
				$column = "timestamp('" . $declare['name'] . "')";
				break;
			case 'datetime':
				$column = "datetime('" . $declare['name'] . "')";
				break;
			case 'boolean':
				$column = "boolean('" . $declare['name'] . "')";
				break;
			case 'blob':
			case 'mediumblob':
				$column = "binary('" . $declare['name'] . "')";
				break;
			case 'date':
				$column = "date('" . $declare['name'] . "')";
				break;
			case 'dateTime':
				$column = "dateTime('" . $declare['name'] . "')";
				break;
			case 'longtext':
				$column = "longText('" . $declare['name'] . "')";
				break;
			case 'mediuminteger':
				$column = "mediumInteger('" . $declare['name'] . "')";
				break;
			case 'mediumText':
				$column = "mediumText('" . $declare['name'] . "')";
				break;
			case 'smallinteger':
				$column = "smallInteger('" . $declare['name'] . "')";
				break;
			case 'tinyinteger':
			case 'tinyint':
				$column = "tinyInteger('" . $declare['name'] . "')";
				break;
			case 'text':
				$column = "text('" . $declare['name'] . "')";
				break;
			case 'time':
				$column = "('" . $declare['name'] . "')";
				break;
				
			default:
				echo "Unknowntype: " . $declare['type'] . "\n";
				print_r($declare);
				die();
				break;
		}
		if ($declare['unsigned'])
			$column .= "->unsigned()";
		if ($declare['nullable'])
			$column .= "->nullable()";
		if (isset($declare['default'])) {
			if ($declare['name'] != 'created_at' && $declare['name'] != 'updated_at')
				$column .= "->default('" .$declare['default']. "')";
		}
		return $column;
	}
				
}