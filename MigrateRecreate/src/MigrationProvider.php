<?php

namespace Thrasher\MigrateRecreate;

use Illuminate\Support\ServiceProvider;

class MigrationProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function setConsole() {
    	if ($this->app->runningInConsole()) {
    		$this->commands([
    				\Thrasher\MigrateRecreate\Console\Commands\MigrateRecreateCommand::class,
    				\Thrasher\MigrateRecreate\Console\Commands\RecreateAllMigrationsCommand::class,
    				
    		]);
    	}
    }
    	
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->setConsole();
    }
}
