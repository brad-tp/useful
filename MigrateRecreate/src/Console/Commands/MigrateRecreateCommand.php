<?php

namespace Thrasher\MigrateRecreate\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Thrasher\MigrateRecreate\RecreateMigrationTable;
use Illuminate\Support\Facades\View;

class MigrateRecreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:recreate {name} {table} {--type=} {--table2=} {{--override}} {{--id1=}} {{--id2=}}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Migration from an existing table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $args = $this->arguments();
      	$mr = new RecreateMigrationTable($args['name'],$args['table'], $this->options());
      	$mr->recreate();
    }
}
