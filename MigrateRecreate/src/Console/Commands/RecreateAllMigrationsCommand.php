<?php

namespace Thrasher\MigrateRecreate\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class RecreateAllMigrationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:recreateall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    function yesOrNo($message) {
    	while (true) {
    		$ask = $this->ask($message);
    		echo "[$ask]";
    		if (empty($ask))
    			$ask = 'Y';
    		if ($ask == 'y' || $ask == 'Y')
    			return true;
    		if ($ask == 'n' || $ask == 'N')
    			return false;
    	}
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $table = "members";
        $name = "create_" . $table . "_table";
        $conn = Schema::getConnection()->getDoctrineSchemaManager();;
        $tables = $conn->listTableNames();
        foreach($tables as $table) {
        	echo "$table\n";
        	$create = true;
        	$x = DB::select('select * from migrations where migration like "%create_' . $table .'%"');
        	if (!empty($x)) {
        		$ask = $this->yesOrNo('Possible Migration for ' .$table.' exists as: ' . $x[0]->migration . ' Create? [Y|n]');
        		if (!$ask)
        			$create = false;
        	}
        	if (!$create)
        		echo "Not creating\n";
        	else {
        		$name = "create_" . $table . "_table";
        		 $this->call("migrate:recreate", [
        		 'name' => $name,
        		 'table' => $table,
        		 '--type' => 'nocreate'
        		 ]);
        	}
        }
       /*
        $this->call("migrate:recreate", [
        	'name' => $name,
        	'table' => $table,
        	'--type' => 'nocreate'
        ]);
        */
    }
}
