@php

function createColumnMethodString($declare) {
	return \Thrasher\MigrateRecreate\RecreateMigrationTable::createColumnMethodString($declare);
}

echo "<?php";
@endphp


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class {{ $name }} extends Migration
{
	use Migratable;
	
	protected $table = '{{ $table }}';
	
	protected $Columns = [
		@foreach ($columns as $key => $value)
			"{{ $key }}",
		@endforeach
	];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->prepare();
        Schema::create($this->table, function (Blueprint $table) {
			@foreach ($columns as $column) 
		$table->{!! createColumnMethodString($column) !!};
			@endforeach

            $this->addExtraColumns($table);
        });
        $this->populate_old_data(function($row) {
			return $row;
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
        $this->rollback();
    }
}
