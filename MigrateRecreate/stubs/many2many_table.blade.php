@php

function createColumnMethodString($declare) {
	return \Thrasher\MigrateRecreate\RecreateMigrationTable::createColumnMethodString($declare);
}
	if (!isset($id1))
		$id1 = str_singular($table1) . "_id";
	if (!isset($id2))
		$id2 = str_singular($table2) . "_id";
	
echo "<?php";
@endphp


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class {{ $name }} extends Migration
{
	use Migratable;
	protected $table = '{{ $table }}';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	$this->createManyToMany('{{$table1}}','{{$table2}}',function ($table) {
			@foreach ($columns as $key=>$column) 
				// {{ $key }}, {{ $id1 }}, {{ $id2 }}
				@if ($id1 != $key && $id2 != $key)
					$table->{!! createColumnMethodString($column) !!};
				@endif
			@endforeach

		}, {{ $table }});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
        $this->rollback();
    }
}
