@php

echo "<?php";
@endphp


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Owens\Traits\Migratable;

class {{ $name }} extends Migration
{
	use Migratable;
	
	protected $table = '{{ $table }}';
	
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    if (!Schema::hasTable($this->table)) {
        	Schema::create($this->table, function (Blueprint $table) {
			@foreach ($columns as $column) 
				$table->{!! \Thrasher\MigrateRecreate\RecreateMigrationTable::createColumnMethodString($column) !!};
			@endforeach
			@if (isset($foreigns))
			@foreach ($foreigns as $foreign) 
				$table->{!! $foreign !!};
			@endforeach
			@endif
			@if (isset($idxs))
			@foreach ($idxs as $idx)
				$table->{!!$idx!!};
			@endforeach
			@endif
        });

		}

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
