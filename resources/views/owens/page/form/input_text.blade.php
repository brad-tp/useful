
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
	<label for="name" class="form-control-label">{{ $label }}</label>

		<div class="">
        	<input id="{{ $name }}" type="text" class="form-control" name="{{ $name }}" value="{{ old($name) }}" @if (isset($required) && $required == true) required @endif autofocus>

             @if ($errors->has($name))
            	<span class="help-block">
                	<strong>{{ $errors->first($name) }}</strong>
                 </span>
			@endif
	</div>
</div>
