@php
	$states = array("AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC",
			"FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME",
			"MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY",
			"NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT",
			"VA", "WA", "WV", "WI", "WY");
@endphp

		<SELECT @if (isset($class)) class="{{ $class }}" @endif name="{{ $name }}">
		<option value=""> Select </option>
		@foreach($states as $state) 
			<OPTION VALUE="{{ $state }}"
				@if ($old_state == $state)
					selected="selected"
				@endif
			>
				{{ $state }} 
		    </OPTION>
		@endforeach
		
		</select>
		
